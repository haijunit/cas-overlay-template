
## 准备需要导入的SQL语句
cat >> jdbc.sql <<EOF

create table sys_user (
 `id` int(11) not null auto_increment,
 `username` varchar(30) not null,
 `password` varchar(64) not null,
 `expired` int,     -- 是否过期 0正常 1过期
 `disabled` int,    -- 是否禁用 0正常 1禁用
 `locked` int,      -- 是否锁定 0正常 1锁定
  primary key (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ;

insert into sys_user values ('1','admin','e10adc3949ba59abbe56e057f20f883e',0, 0, 0);

EOF

# 注释掉静态认证
sed -i 's/cas.authn.accept.users/# cas.authn.accept.users/' ../application.properties

# 添加JDBC登录配置
cat >> ../application.properties <<EOF

#添加jdbc认证
cas.authn.jdbc.query[0].sql=select * from sys_user where username = ?
cas.authn.jdbc.query[0].fieldPassword=password
cas.authn.jdbc.query[0].fieldExpired=expired
cas.authn.jdbc.query[0].fieldDisabled=disabled
#配置数据库连接
cas.authn.jdbc.query[0].dialect=org.hibernate.dialect.MySQLDialect
cas.authn.jdbc.query[0].driverClass=com.mysql.jdbc.Driver
cas.authn.jdbc.query[0].url=jdbc:mysql://127.0.0.1:3306/cas?characterEncoding=utf8&useSSL=true
cas.authn.jdbc.query[0].user=root
cas.authn.jdbc.query[0].password=123456

# 配置加密策略，默认NONE不加密
cas.authn.jdbc.query[0].passwordEncoder.type=DEFAULT
cas.authn.jdbc.query[0].passwordEncoder.characterEncoding=UTF-8
cas.authn.jdbc.query[0].passwordEncoder.encodingAlgorithm=MD5

EOF