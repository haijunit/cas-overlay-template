#!/bin/bash
# 改造Ldap登录
set -e

ip=$((ip addr||ifconfig) |grep -v 255.0.0.0|grep -v 255.255.0.0|grep -v docker| sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' |head -1)

echo "本机IP地址: $ip"

read -r -p "请确定Ldap地址(ldap://$ip:389)正确,是否继续? [Y/n]" response
response=${response,,} # tolower
if [[ ! $response =~ ^(yes|y) ]] ; then
   echo "结束,Ldap未配置"
   exit 0;
fi

# 注释掉静态认证
sed -i 's/cas.authn.accept.users/# cas.authn.accept.users/' ../application.properties
# 添加Ldap登录配置
cat >> ../application.properties <<EOF

###################ldap authentication######################
cas.authn.ldap[0].principalAttributeList=sn,cn:commonName,givenName,eduPersonTargettedId:SOME_IDENTIFIER
cas.authn.ldap[0].collectDnAttribute=false
cas.authn.ldap[0].principalDnAttributeName=principalLdapDn
cas.authn.ldap[0].allowMultiplePrincipalAttributeValues=true
cas.authn.ldap[0].allowMissingPrincipalAttributeValue=true
cas.authn.ldap[0].credentialCriteria=

cas.authn.ldap[0].ldapUrl=ldap://$ip:389
cas.authn.ldap[0].bindDn=cn=admin,dc=txra,dc=com
cas.authn.ldap[0].bindCredential=123456
cas.authn.ldap[0].baseDn=ou=bjtxra,o=txra,dc=txra,dc=com

cas.authn.ldap[0].poolPassivator=NONE
cas.authn.ldap[0].connectionStrategy=
cas.authn.ldap[0].providerClass=org.ldaptive.provider.unboundid.UnboundIDProvider
cas.authn.ldap[0].connectTimeout=PT5S
cas.authn.ldap[0].trustCertificates=
cas.authn.ldap[0].keystore=
cas.authn.ldap[0].keystorePassword=
cas.authn.ldap[0].keystoreType=JKS
cas.authn.ldap[0].minPoolSize=3
cas.authn.ldap[0].maxPoolSize=10
cas.authn.ldap[0].validateOnCheckout=true
cas.authn.ldap[0].validatePeriodically=true
cas.authn.ldap[0].validatePeriod=PT5M
cas.authn.ldap[0].validateTimeout=PT5S
cas.authn.ldap[0].failFast=true
cas.authn.ldap[0].idleTime=PT10M
cas.authn.ldap[0].prunePeriod=PT2H
cas.authn.ldap[0].blockWaitTime=PT3S
cas.authn.ldap[0].useSsl=false
cas.authn.ldap[0].useStartTls=false
cas.authn.ldap[0].responseTimeout=PT5S
cas.authn.ldap[0].allowMultipleDns=false
cas.authn.ldap[0].allowMultipleEntries=false
cas.authn.ldap[0].followReferrals=false
cas.authn.ldap[0].binaryAttributes=objectGUID,someOtherAttribute
cas.authn.ldap[0].name=
cas.authn.ldap[0].type=AUTHENTICATED
cas.authn.ldap[0].searchFilter=(|(uid={user})(mail={user})(mobile={user}))

EOF