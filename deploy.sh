#!/bin/bash


cd `dirname $0`
DeployDir=`pwd`
echo "当前目录：$DeployDir"

DEPLOP_DIR=build/CAS-5.3

mvn clean package -Dmaven.test.skip=true
if [[ $? -ne 0 ]] ; then
  echo Error: build Fail
  exit 1
fi

## 准备部署文件
rm -rf $DEPLOP_DIR && mkdir -p $DEPLOP_DIR

cp -avf target/cas.war $DEPLOP_DIR/
cp -avf docker/* $DEPLOP_DIR/
cp -avf etc $DEPLOP_DIR/
cp -avf build.sh $DEPLOP_DIR

echo "------ 编译完毕，编译文件：$DeployDir/$DEPLOP_DIR ------"
