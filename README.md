## CAS 部署
> CAS作为一款成熟的开源SSO系统，应该部署起来非常容易。但实际部署过程中，因为众所周知的网络原因，没少花费时间，最终探索出来一条顺利的渠道。

#### 方法一：docker部署
> 太慢了~~ 网络不行的话就不要用这个方法了
```shell
docker pull apereo/cas:v5.3.10
docker run --name cas -p 8443:8443 -p 8878:8080 apereo/cas:v5.3.10 /bin/sh /cas-overlay/bin/run-cas.sh
```

#### 方法二：下载CAS 源码，编译打包方式。（不推荐，源码太大，下载不下来）
```shell
## 源码地址：https://github.com/apereo/cas
git clone https://github.com/apereo/cas.git
```

#### 方法三：war overlay installation 方式 （推荐，此方式顺利完成）
```text
War-Overlay 是一种避免重复代码的简化部署工作策略，overlay无需从cas源代码开始构建，而是允许下载预编译好的war包，并将自己特性的内容（覆盖物）插入或覆盖其中。可以覆盖的组件可能包括资源文件、java类、图像、css样式、js文件等。如果是覆盖预编译war中的内容，则要在工程中建立完全相同的路径和文件名称。
```

## overlay方式部署CAS Server（推荐）
```shell
## JDK版本：JDK 1.8
## 依赖CAS版本：5.3.x
git clone -b 5.3 git clone https://github.com/apereo/cas-overlay-template.git
mvn clean compile package
## 查看本地生成命令
./build.sh help
## 生成HTTPS密钥
./build.sh gencert
./build.sh copy
## 本地启动项目
./build.sh run
./build.sh bootrun
## To package the final web application, run:
./build.sh package
## To update SNAPSHOT versions run:
./build.sh package -U
## Invokes the CAS Command Line Shell. For a list of commands either use no arguments or use `-h`. To enter the interactive shell use `-sh`.
./build.sh cli
```
* `http://cas.server.name:8080/cas`
* `https://cas.server.name:8443/cas`
